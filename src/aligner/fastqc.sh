#!/usr/bin/env bash
#$ -S /bin/bash
#$ -cwd
#$ -M yun.yan@nyumc.org
#$ -m e
#$ -l h_vmem=30G
#$ -l mem_token=10G
#$ -N fastqc

OUTDIR="/ifs/home/yy1533/Projects/challenge/res/fastqc"
if [ ! -d "$OUTDIR" ]; then
    mkdir -p $OUTDIR
fi

for fa in `ls /ifs/home/yy1533/DATA/challenge/*.gz`
do
    fa_name=${fa##*/}
    fa_basename=${fa_name%.fastq.gz}
    echo $fa_name
    echo $fa_basename
    fastqc $fa --outdir $OUTDIR

    date
done